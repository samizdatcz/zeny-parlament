---
title: "Genderově vyvážený parlament? Poslankyně převažují jen ve dvou zemích světa. Česko je na tom jako arabské státy"
perex: "Ženy tvoří necelou pětinu ze dvou set zákonodárců v Poslanecké sněmovně. Poměr mužů a žen mezi českými poslanci se přitom za posledních dvacet let změnil jen nepatrně. V žebříčku podílu žen ve světových parlamentech se tak Česko ocitá v polovině&nbsp;–&nbsp;za Saúdskou Arábií, ale před USA."
description: "Z dvou set zákonodárců v české Poslanecké sněmovně tvoří ženy necelou pětinu. Poměr mužů a žen mezi českými poslanci se přitom za posledních dvacet let změnil jen nepatrně. V žebříčku podílu žen ve světových parlamentech se tak Česko ocitá v polovině – za Saúdskou Arábií, ale před USA."
authors: ["Michal Zlatkovký", "Jan Cibulka"]
published: "23. března 2017"
socialimg: https://interaktivni.rozhlas.cz/zeny-parlament/media/cover.jpg
url: "zeny-parlament"
libraries: [jquery, highcharts, "//cdnjs.cloudflare.com/ajax/libs/d3/3.5.3/d3.min.js", "//cdnjs.cloudflare.com/ajax/libs/topojson/1.6.9/topojson.min.js", "./data/datamap.js"]
styles: ["./styl/mapa.css"]
recommended:
  - link: https://interaktivni.rozhlas.cz/alkoholismus/
    title: "Alkohol zabíjí dvakrát víc žen než před dvaceti lety"
    perex: "Ještě v polovině devadesátých let měli na problémy s alkoholem monopol muži. Dnes je začínají dotahovat ženy. Kritický je věk 35 až 44 let."
    image: https://interaktivni.rozhlas.cz/alkoholismus/media/cover.jpg
  - link: https://interaktivni.rozhlas.cz/porody/
    title: "Data: O víkendu a v noci se rodí méně dětí. Kvůli císařským řezům"
    perex: "Nejvíc novorozenců přichází na svět v létě - a nejméně o sobotách a nedělích. V noci se zase narodí sotva polovina dětí oproti dopoledni. Jde o rozhodnutí matek, nebo si plánováním termínů ulehčují práci porodníci? Podívejte se na podrobná data o porodech v roce 2015."
    image: https://interaktivni.rozhlas.cz/porody/media/socialimg.jpg
  - link: https://interaktivni.rozhlas.cz/krajske-kandidatky/
    title: "Chybí budoucí hejtmanky, nastupují protisystémové strany: prohlédněte si krajské kandidátky v grafech"
    perex: "Sociální demokraté postaví do voleb překvapivě mnoho žen, ale žádnou lídryni. ANO bude mít kandidátky téměř stoprocentně z politických nováčků a křesťanští demokraté jsou nejvzdělanější. Co o stranách říkají kandidátky?"
    image: https://interaktivni.rozhlas.cz/krajske-kandidatky/media/socimg.png
---

<div data-bso="1"></div>

Podíl žen v parlamentu stejně jako v Česku nepřesahuje 20 procent ani na Slovensku, v Řecku nebo Chorvatsku. V zemích západní Evropy se pohybuje mezi čtvrtinou a 40 procenty. Z desítky států na špici žebříčku jsou však evropské jen tři: Island, Švédsko a Finsko. 

Více než polovinu žen má ve svých zákonodárných sborech pouze africká Rwanda a jihoamerická Bolívie. U Rwandy, kde je ženské zastoupení více než šedesátiprocentní, jde navíc o [zvláštní případ](http://www.ibtimes.com/rwanda-only-government-world-dominated-women-213623): v devadesátých letech zemřela během rwandské genocidy pětina celé populace, často muži v produktivním věku. Přežilo je 50 tisíc vdov. 

<aside class="big">
  <div id="mapa" style="height: 75vh; position: relative;"></div>
</aside>
 
Data o složení národních parlamentů [sleduje Meziparlamentní unie](http://www.ipu.org/wmn-e/classif.htm), organizace spolupracující s OSN. Česko je aktuálně s 19,5procentním zastoupením žen v Poslanecké sněmovně v žebříčku 193 zemí na 98. místě. Nejvíce žen, celkem 44, bylo do sněmovny zvoleno v roce 2010.

Přestože se podíl žen ve vedoucích pozicích stává tématem nejen neziskových organizací jako je [Fórum 50 %](http://padesatprocent.cz/cz/statistiky-analyzy-vyzkumy/zastoupeni-zen-a-muzu-v-politice/), ale i [aktuálně nejsilnější vládní strany](https://zpravy.aktualne.cz/domaci/zeny-vpred-cssd-schvalila-kvoty-pro-kandidatky/r~592f89aeca3911e4a4c00025900fea04/), poměr žen a mužů v parlamentu se od roku 1996 výrazně nezměnil. 

<aside class="big">
<div id="zenyCesko" style="max-width: 600px; margin: auto"></div>
</aside>

Poslankyně v Dánsku, Švédsku, Norsku, Finsku i na Islandu tvoří kolem 40 procent zákonodárného sboru. V arabských státech je oproti tomu průměr asi poloviční. Zastoupení žen je tam zhruba 19procentní, tedy podobné jako v Česku. Žádná žena nezasedá v parlamentu čtyř zemí: Kataru, Jemenu, Mikronésie a Vanuatu.

<aside class="big">
<div id="svet" style="max-width: 600px; margin: auto"></div>
</aside>