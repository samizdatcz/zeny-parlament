from lxml import html
import requests, csv
page = requests.get('http://www.ipu.org/wmn-e/classif.htm')
tree = html.fromstring(page.content)
country = tree.xpath('//tr[@class="odd"]/td[2]/text()')
total_women = tree.xpath('//tr[@class="odd"]/td[5]/text()')
total_all = tree.xpath('//tr[@class="odd"]/td[4]/text()')
dataset_header = ["country","pct","total_women","total_seats"]
dataset = []
for i,n in enumerate(country):
	pct = round((int(total_women[i])/int(total_all[i])*100), 1)
	dataset.append([str.strip(country[i]),pct,total_women[i],total_all[i]])
with open("data.csv", "w", newline='') as output:
	writer = csv.writer(output, delimiter=',')
	writer.writerow(dataset_header)
	for row in dataset:
			writer.writerow(row)