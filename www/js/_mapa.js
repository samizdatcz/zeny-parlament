var colorScale = d3.scale.linear()
          .domain([7.5, 14.9, 24.2, 35.1])
          .range(['#f2f0f7', '#cbc9e2', '#9e9ac8', '#756bb1'])

    $.getJSON( "./data/zeny.json", function( dataset ) {
      for (var i in dataset) {
        dataset[i].fillColor = colorScale(dataset[i].pct)
      };

      var map = new Datamap({
          element: document.getElementById('mapa'),
          projection: 'mercator',
          responsive: true,
          dataType: 'json',
          data: dataset,
          fills: { defaultFill: '#f0f0f0' },
          highlightFillColor: function(geo) {
                return geo['fillColor'] || '#F5F5F5';
          },
          highlightBorderColor: '#B7B7B7',
          geographyConfig: {
            borderColor: 'white',
            popupTemplate: function(geo, data) {
                // don't show tooltip if country don't present in dataset
                if (!data) { return ; }
                // tooltip
                return ['<div id="infobox" style="position: absolute;">',
                    '<strong>', data.name, '</strong>',
                    '<br>V parlamentu je zde <strong>' + data.pct.toString().replace(".",",") + ' %</strong> žen (' + data.total_women + ' z celkem ' + data.total_seats + ' křesel).',
                    '</div>'].join('');
            },
            popupOnHover: true,
            highlightOnHover: false,
          }
          
      });
      
      window.addEventListener('resize', function() {
        map.resize();
      });
    }); 