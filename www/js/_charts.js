Highcharts.setOptions({
    lang: {
        decimalPoint: "," ,
        }
    });

Highcharts.chart('zenyCesko', {
    chart: {
        type: 'column'
    },
    colors: ['#222D55'],
    title: {
        text: 'Ženy v českém parlamentu'
    },
    subtitle: {
        text: 'Podíl žen z 200 zvolených poslanců'
    },
    credits: {
        text: 'data: ČSÚ',
        href: 'http://volby.cz'
    },  
    xAxis: {
        categories: [
            '1996',
            '1998',
            '2002',
            '2006',
            '2010',
            '2013'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        max: 50,
        title: {
            text: 'Podíl žen (%)'
        },
        labels: {
            formatter: function() {
            return this.value+" %";
            }
        }        
    },
    tooltip: {
        formatter: function () {
            return 'V roce <b>' + this.x +
                '</b> bylo do Poslanecké sněmovny<br>zvoleno <b>' + this.y.toString().replace(".",",") + ' % žen</b> (' + this.y*2 + ' z 200 poslanců).';
        }        
    },
    plotOptions: {
        column: {
            pointPadding: 0.05,
            borderWidth: 0
        }
    },
    legend: {
        enabled: false
    },
    series: [{
        data: [15, 15, 17, 15.5, 22, 19.5]
    }]
});

Highcharts.chart('svet', {
    chart: {
        type: 'column'
    },
    colors: ['#222D55'],    
    title: {
        text: 'Ženy ve světových parlamentech'
    },
    subtitle: {
        text: 'Podíl zvolených žen'
    },
    credits: {
        text: 'data: IPU',
        href: 'http://www.ipu.org/wmn-e/world.htm'
    },  
    xAxis: {
        categories: [
            'Skandinávie',
            'Evropský parlament',
            'Severní a Jižní Amerika',
            'Evropa (bez Skandinávie)',
            'subsaharská Afrika',
            'Asie',
            'Česko',
            'arabské státy'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Podíl žen (%)'
        },
        labels: {
            formatter: function() {
            return this.value+" %";
            }
        },
        plotLines: [{
            value: 23.4,
            color: '#666666',
            dashStyle: 'dot',
            width: 1,
            label: {
                text: 'celosvětový průměr',
                align: 'right'
                }
        }]               
    },
    tooltip: {
        formatter: function () {
            return this.x +
                ': <b>' + this.y.toString().replace(".",",") + ' % žen</b>';
        }        
    },
    plotOptions: {
        column: {
            pointPadding: 0.05,
            borderWidth: 0
        }
    },
    legend: {
        enabled: false
    },
    series: [{
        data: [41.7, 35.2, 28.2, 24.9, 23.8, 19.7, 19.5, 18.9]
    }]
});